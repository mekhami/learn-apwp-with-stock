from sqlalchemy.orm import relationship, mapper
from sqlalchemy import MetaData, Table, Column, String, Integer, ForeignKey

from stock.model import Ledger, Location


metadata = MetaData()

ledger = Table(
    'ledgers', metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('ledgerid', String(255)),
    Column('sku', String(255)),
    Column('condition', String(255), default='pristine'),
    Column('status', String(255), default='on_hand'),
    Column('total', Integer, default=0),
    Column('location_id', Integer, ForeignKey('locations.id'))
)

location = Table(
    'locations', metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('name', String(255)),
)


def start_mappers():
    locations_mapper = mapper(Location, location)
    mapper(Ledger, ledger, properties={
        'location': relationship(locations_mapper)
    })
