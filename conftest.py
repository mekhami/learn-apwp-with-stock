import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, clear_mappers

from orm import metadata, start_mappers
from stock.repository import AbstractRepository
from stock.unit_of_work import AbstractUnitOfWork
from stock import model


@pytest.fixture
def in_memory_db():
    engine = create_engine('sqlite:///:memory:')
    metadata.create_all(engine)
    return engine


@pytest.fixture
def session_factory(in_memory_db):
    start_mappers()
    yield sessionmaker(bind=in_memory_db)
    clear_mappers()


@pytest.fixture
def session(session_factory):
    return session_factory()


class FakeRepository(AbstractRepository):
    def __init__(self, ledgers, locations):
        self._ledgers = set(ledgers)
        self._locations = set(locations)

    def add_ledger(self, ledger):
        self._ledgers.add(ledger)

    def add_location(self, location):
        self._locations.add(location)

    def get_ledger(self, ledgerid):
        return next(ldg for ldg in self._ledgers if ldg.ledgerid == ledgerid)

    def get_location(self, location_name):
        return next(loc for loc in self._locations if loc.name == location_name)

    def list_ledgers(self):
        return list(self._ledgers)

    def get_or_create_ledger(self, defaults=None, **kwargs):
        exists = next((
            ldg for ldg in self._ledgers
            if all([
                getattr(ldg, kwarg) == kwargs.get(kwarg)
                for kwarg in kwargs
            ])
        ), None)
        kwargs |= defaults or {}
        return exists if exists else model.Ledger(**kwargs)


class FakeUnitOfWork(AbstractUnitOfWork):
    def __init__(self):
        self.ledgers = FakeRepository([], [])
        self.committed = False

    def commit(self):
        self.committed = True

    def rollback(self):
        pass
