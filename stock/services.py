from stock import model, unit_of_work


def record_movement(
    ledgerid: str,
    quantity: int,
    to_location: str,
    strict: bool = False,
    uow: unit_of_work.AbstractUnitOfWork = None
) -> model.Transaction:
    with uow:
        origin = uow.ledgers.get_ledger(ledgerid)
        new_location = uow.ledgers.get_location(to_location)

        destination = uow.ledgers.get_or_create_ledger(
            sku=origin.sku,
            condition=origin.condition,
            status=origin.status,
            location=new_location
        )
        transaction = model.Transaction(
            origin=origin,
            destination=destination,
            quantity=quantity,
            strict=strict
        )
        new_origin, new_destination = transaction.move()
        uow.ledgers.add_ledger(new_origin)
        uow.ledgers.add_ledger(new_destination)
        uow.commit()
    return transaction


def initialize_ledger(
    sku: str,
    location_name: str,
    total: int,
    condition: str = "pristine",
    status: str = "on_hand",
    uow: unit_of_work.AbstractUnitOfWork = None,
) -> model.Ledger:
    with uow:
        ledger = model.Ledger(
            sku=sku,
            location=uow.ledgers.get_location(location_name),
            total=total,
            condition=condition,
            status=status
        )
        uow.ledgers.add_ledger(ledger)
        uow.commit()
    return ledger


def create_location(
    name: str,
    uow: unit_of_work.AbstractUnitOfWork,
) -> model.Location:
    location = model.Location(name=name)
    with uow:
        uow.ledgers.add_location(location)
        uow.commit()
    return location
