import abc
from typing import List

from stock import model


class AbstractRepository(abc.ABC):
    @abc.abstractmethod
    def add_ledger(self, ledger: model.Ledger) -> model.Ledger:
        raise NotImplementedError

    @abc.abstractmethod
    def get_ledger(self, ledgerid: str) -> model.Ledger:
        raise NotImplementedError

    @abc.abstractmethod
    def add_location(self, location: model.Location) -> model.Location:
        raise NotImplementedError

    @abc.abstractmethod
    def get_location(self, name: str) -> model.Location:
        raise NotImplementedError

    @abc.abstractmethod
    def list_ledgers(self) -> List[model.Ledger]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_or_create_ledger(self) -> model.Ledger:
        raise NotImplementedError


class SqlARepository(AbstractRepository):
    def __init__(self, session):
        self.session = session

    def add_ledger(self, ledger):
        self.session.add(ledger)

    def get_ledger(self, ledgerid):
        return self.session.query(model.Ledger).filter_by(ledgerid=ledgerid).one()

    def list_ledgers(self):
        return self.session.query(model.Ledger).all()

    def add_location(self, location):
        self.session.add(location)

    def get_location(self, name):
        return self.session.query(model.Location).filter_by(name=name).one()

    def get_or_create_ledger(self, defaults=None, **kwargs):
        existing = self.session.query(model.Ledger).filter_by(**kwargs).one_or_none()
        if existing:
            return existing
        else:
            kwargs |= defaults or {}
            ledger = model.Ledger(**kwargs)
            self.session.add(ledger)
            return ledger
