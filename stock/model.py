from dataclasses import dataclass, field
from typing import Optional
import random
import string


@dataclass(unsafe_hash=True)
class Location:
    id: int = field(default_factory=int, init=False, compare=False)
    name: str


def random_string():
    # TODO: Replace this with UUID generation once an actual
    # dev database is in place and we're not just on sqlite in memory
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=15))


@dataclass(unsafe_hash=True)
class Ledger:
    id: int = field(default_factory=int, init=False, compare=False)
    sku: str
    condition: str
    status: str
    location: Location
    total: int = field(default_factory=int)
    ledgerid: str = field(default_factory=random_string)


class InsufficientStock(Exception):
    pass


class Transaction:
    def __init__(
        self,
        origin: Ledger,
        destination: Ledger,
        quantity: int,
        reason: Optional[str] = None,
        strict: bool = False
    ):
        self.origin = origin
        self.destination = destination
        self.quantity = quantity
        self.reason = reason
        self.strict = strict

    def can_move(self):
        return not self.origin.total < self.quantity

    def move(self):
        if self.strict and not self.can_move():
            raise InsufficientStock

        self.new_origin, self.new_destination = self._move(
            self.origin, self.destination
        )
        return self.new_origin, self.new_destination

    def _move(self, origin, destination):
        origin = Ledger(
            sku=origin.sku,
            condition=origin.condition,
            status=origin.status,
            location=origin.location,
            total=origin.total - self.quantity,
            ledgerid=origin.ledgerid
        )
        destination = Ledger(
            sku=destination.sku,
            condition=destination.condition,
            status=destination.status,
            location=destination.location,
            total=destination.total + self.quantity,
            ledgerid=destination.ledgerid
        )
        return origin, destination
