import pytest
from stock import services, model
from conftest import FakeUnitOfWork


def test_cannot_move_insufficient_stock_in_strict_mode():
    uow = FakeUnitOfWork()
    location_1 = services.create_location('Delta-1', uow)
    location_2 = services.create_location('Delta-2', uow)

    ledger = services.initialize_ledger(
        "sku-1",
        location_1.name,
        10,
        uow=uow
    )

    with pytest.raises(model.InsufficientStock):
        services.record_movement(
            ledger.ledgerid,
            20,
            to_location=location_2.name,
            strict=True,
            uow=uow
        )


def test_move_from_one_location_to_another():
    uow = FakeUnitOfWork()
    location_1 = services.create_location('Delta-1', uow)
    location_2 = services.create_location('Delta-2', uow)

    ledger = services.initialize_ledger(
        "sku-1",
        location_1.name,
        20,
        uow=uow
    )

    transaction = services.record_movement(
        ledger.ledgerid,
        10,
        to_location=location_2.name,
        uow=uow
    )

    assert transaction.new_origin.total == 10
    assert transaction.new_destination.total == 10
    assert transaction.destination.location.name == 'Delta-2'
    assert uow.committed is True


def test_move_origin_has_same_ledgerid_as_new_entry():
    uow = FakeUnitOfWork()
    location_1 = services.create_location('Delta-1', uow)
    location_2 = services.create_location('Delta-2', uow)

    ledger = services.initialize_ledger(
        "sku-1",
        location_1.name,
        20,
        uow=uow
    )

    transaction = services.record_movement(
        ledger.ledgerid,
        10,
        to_location=location_2.name,
        uow=uow
    )
    assert transaction.new_origin.ledgerid == transaction.origin.ledgerid
    assert transaction.new_destination.ledgerid == transaction.destination.ledgerid
    assert transaction.new_origin.ledgerid != transaction.new_destination.ledgerid


def test_initialize_ledger():
    uow = FakeUnitOfWork()
    location_1 = services.create_location('Delta-1', uow)

    ledger = services.initialize_ledger(
        "sku-1",
        location_1.name,
        20,
        uow=uow
    )
    assert uow.committed is True
    assert ledger.location == location_1


def test_create_location():
    uow = FakeUnitOfWork()
    services.create_location('Delta-1', uow)
    assert uow.ledgers.get_location("Delta-1") is not None
    assert uow.committed
