from stock import model, repository
from tests.test_transactions import make_ledger


def test_repository_can_save_a_ledger(session):
    ledger = make_ledger(ledgerid='ledger-001')

    repo = repository.SqlARepository(session)
    repo.add_ledger(ledger)
    session.commit()

    rows = list(session.execute('SELECT sku FROM "ledgers"'))
    assert rows == [(ledger.sku,)]


def insert_ledger(session, ledgerid="ledger-001", location_id=1):
    session.execute(
        'INSERT INTO ledgers '
        '(ledgerid, sku, condition, status, total, location_id) VALUES '
        f'("{ledgerid}", "sku1", "pristine", "on_hand", 50, {location_id})'
    )
    [[ledgerid]] = session.execute(
        'SELECT ledgerid FROM ledgers WHERE ledgerid=:ledgerid',
        dict(ledgerid=ledgerid)
    )
    return ledgerid


def insert_location(session, name='delta-1'):
    session.execute(
        f'INSERT INTO locations (name) VALUES ("{name}")'
    )
    [[location_id]] = session.execute(
        'SELECT id FROM locations'
    )
    return location_id


def test_repository_can_retrieve_ledger(session):
    location_id = insert_location(session)
    ledgerid = insert_ledger(session, location_id=location_id)
    repo = repository.SqlARepository(session=session)
    [[ledgerid, total]] = session.execute(
        'SELECT ledgerid, total FROM ledgers WHERE ledgerid=:ledgerid',
        dict(ledgerid=ledgerid)
    )

    ledger = repo.get_ledger(ledgerid)
    expected = model.Ledger(
        "sku1",
        "pristine",
        "on_hand",
        model.Location("delta-1"),
        total=50,
    )
    assert ledger.total == expected.total
    assert ledger.sku == expected.sku
    assert ledger.total == expected.total
    assert ledger.location == expected.location
