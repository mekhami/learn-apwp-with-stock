from stock import unit_of_work


def insert_ledger(session, sku, condition, status, location_id, total, ledgerid):
    session.execute(
        'INSERT INTO ledgers '
        '(sku, condition, status, location_id, total, ledgerid) VALUES '
        f'("{sku}", "{condition}", "{status}", {location_id}, {total}, "{ledgerid}")'
    )


def get_ledger(session, ledgerid):
    [[id]] = session.execute(
        'SELECT ledgerid FROM ledgers WHERE ledgerid=:ledgerid',
        dict(ledgerid=ledgerid)
    )
    return id


def test_uow_can_retrieve_a_ledger_and_update_it(session_factory):
    session = session_factory()
    insert_ledger(
        session,
        'sku-1',
        'pristine',
        'on_hand',
        '1',
        50,
        'ledger-1'
    )
    session.commit()

    uow = unit_of_work.SqlAUnitOfWork(session_factory)
    with uow:
        ledger_id = uow.ledgers.get_ledger(ledgerid='ledger-1').ledgerid
        # ledger.correct_total(30)
        uow.commit()

    ledgerid = get_ledger(session, ledger_id)
    assert ledgerid == 'ledger-1'
