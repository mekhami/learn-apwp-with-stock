import pytest

from stock.model import Location, Ledger


def test_location_mapper_can_load_locations(session):
    session.execute(
        'INSERT INTO locations (name) VALUES '
        '("delta-1"),'
        '("delta-2")'
    )
    expected = [
        Location("delta-1"),
        Location("delta-2")
    ]

    assert session.query(Location).all() == expected


def test_location_mapper_can_save_locations(session):
    location = Location("warehouse-1")
    session.add(location)
    session.commit()

    rows = list(session.execute('SELECT name FROM "locations"'))
    assert rows == [(location.name,)]


def test_ledger_mapper_can_load_ledgers(session):
    session.execute(
        'INSERT INTO locations (name) VALUES ("delta-1")'
    )
    session.execute(
        'INSERT INTO ledgers '
        '(ledgerid, sku, condition, status, total, location_id) VALUES '
        '("ledger-1", "sku1", "pristine", "on_hand", 50, 1),'
        '("ledger-2", "sku2", "pristine", "on_hand", 50, 1)'
    )
    expected = [
        Ledger(
            "sku1",
            "pristine",
            "on_hand",
            Location("delta-1"),
            total=50,
            ledgerid="ledger-1"
        ),
        Ledger(
            "sku2",
            "pristine",
            "on_hand",
            Location("delta-1"),
            total=50,
            ledgerid="ledger-2"
        ),
    ]
    assert session.query(Ledger).all() == expected


def test_ledger_mapper_can_save_ledger(session):
    location = Location("warehouse-1")
    session.add(location)

    ledger = Ledger("sku1", "pristine", "on_hand", location, total=50)
    session.add(ledger)
    session.commit()

    rows = list(session.execute('SELECT ledgerid, location_id FROM "ledgers"'))
    assert rows == [(ledger.ledgerid, location.id)]
