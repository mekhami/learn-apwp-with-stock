from stock.model import Ledger, Transaction, Location


def make_ledger(**options):
    return Ledger(
        sku=options.get('sku', 'fake-sku'),
        condition=options.get('condition', 'pristine'),
        status=options.get('status', 'on_hand'),
        location=options.get('location', Location('delta-1')),
        total=options.get('total', 50)
    )


def test_transaction_can_move():
    origin = make_ledger(total=20)
    destination = make_ledger()

    transaction = Transaction(origin=origin, destination=destination, quantity=20)

    assert transaction.can_move()


def test_transaction_cannot_move():
    origin = make_ledger(total=10)
    destination = make_ledger()

    transaction = Transaction(origin=origin, destination=destination, quantity=20)
    assert not transaction.can_move()
